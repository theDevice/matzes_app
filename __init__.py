import gpio
import leds
import color 
import buttons
from color import Color
import display
import utime
import bhi160

pin_switch = 4
pin_DIYbutton = 3
pin_vibrator = 2

sleepTime = 500 #ms
gpio.set_mode( pin_switch, gpio.mode.INPUT )
gpio.set_mode( pin_DIYbutton, gpio.mode.INPUT )
stateDIYbutton = False
stateDIYbutton_old = False
stateSwitch = False
stateSwitch_old = False
leftButton = False
leftButton_old = False
rightButton = False
rightButton_old = False

dimValue = 1
leds.set_powersave( eco=True )
disp = display.open()
disp.clear().backlight(0).update()

vibrationTime = 30 # ms
gpio.set_mode( pin_vibrator, gpio.mode.OUTPUT )
lastOrientation = [0,0,0,0]
fov = 45 #field of vision
frequenzDividerForPulses = 2
pulseCounter = 0

def updateButtons():
    global stateSwitch
    global stateSwitch_old
    global leftButton
    global leftButton_old
    global rightButton
    global rightButton_old
    global stateDIYbutton
    global stateDIYbutton_old
    stateSwitch_old = stateSwitch
    stateSwitch = gpio.read( pin_switch )
    rightButton_old = rightButton
    rightButton = buttons.read( buttons.BOTTOM_RIGHT )
    leftButton_old = leftButton
    leftButton = buttons.read( buttons.BOTTOM_LEFT )
    stateDIYbutton_old = stateDIYbutton
    stateDIYbutton = gpio.read( pin_DIYbutton )

def checkDimAjustment():
    global stateSwitch
    global leftButton
    global leftButton_old
    global rightButton
    global rightButton_old
    global dimValue
    if stateSwitch == True: #flashlight is on
        if leftButton != 0 and leftButton_old == 0:
            dimValue -= 1
            if dimValue < 1:
                dimValue = 1
        if rightButton != 0 and rightButton_old == 0:
            dimValue += 1
            if dimValue > 8:
                dimValue = 8
        leds.dim_top( dimValue )

def ckeckFlashLight(): 
    global stateSwitch
    global stateSwitch_old
    global dimValue
    if stateSwitch == True and stateSwitch_old == False:
        dimValue = 1
        leds.dim_top( dimValue )
        for number in range( 0, 11 ):
            leds.set( number, color.WHITE )
    if stateSwitch == False and stateSwitch_old == True:
        leds.clear()

def displayOrientation():
    global disp
    string = "x:"+str(int(lastOrientation[0]))+" y:"+str(int(lastOrientation[1]))+" z:"+str(int(lastOrientation[2]))+" q:"+str(lastOrientation[3])
    disp.print( string, font=display.FONT12 )

def displayTime():
    global disp
    time = utime.localtime()
    hours = time[3]
    if hours > 9:
        hours = str( hours )
    else:
        hours = "0" + str( time[3] )
    minutes = time[4]
    if minutes > 9:
        minutes = str( minutes )
    else:
        minutes = "0" + str( time[4] )
    timeString = hours + ":" + minutes
    disp.print( timeString, posx=42, posy=27, font=display.FONT24 )

def checkDIYbutton():
    global stateDIYbutton
    global stateDIYbutton_old
    global disp
    if stateDIYbutton == True and stateDIYbutton_old == False:
        disp.backlight(40)
        displayTime()
        displayOrientation()
        disp.update()
    if stateDIYbutton == False and stateDIYbutton_old == True:
        disp.clear().backlight(0).update()

def pulse( in_vibrationTime ):
    global pin_vibrator
    gpio.write( pin_vibrator, 1 )
    utime.sleep_ms( int(in_vibrationTime) )
    gpio.write( pin_vibrator, 0 )

def middleOrientation( in_listOfDataVectors ):
    numberOfVectors = len( in_listOfDataVectors )
    output = None
    if numberOfVectors > 0:
        output = [0, 0, 0, 0]   #order is: x, y, z, status
        for index in range( 0, numberOfVectors ):
            output[0] += in_listOfDataVectors[index].x
            output[1] += in_listOfDataVectors[index].y
            output[2] += in_listOfDataVectors[index].z
            output[3] += in_listOfDataVectors[index].status
        output[0] = output[0]/numberOfVectors
        output[1] = output[1]/numberOfVectors
        output[2] = output[2]/numberOfVectors
        output[3] = int( output[3]/numberOfVectors + 0.5 )
    return output


def handleSample( in_sample ):
    global vibrationTime
    global fav
    global frequenzDividerForPulses
    global pulseCounter
    if in_sample[3] == 3: #only when exact enough, qos = 3
        degrees = in_sample[0]
        if degrees > 360 - fov / 2 or degrees < fov / 2:
            print("pulse!")
            if pulseCounter == 0:
                pulse( vibrationTime )
            pulseCounter += 1
            pulseCounter %= frequenzDividerForPulses
        else:
            print("nope")
        
def checkOrientation( in_orientation ):
    global lastOrientation
    lastOrientation = in_orientation.read()
    if len(lastOrientation) > 0:
        lastOrientation = middleOrientation( lastOrientation )
        string = str(int(lastOrientation[0]))+" "+str(int(lastOrientation[1]))+" "+str(int(lastOrientation[2]))+" "+str(lastOrientation[3])
        print( string )
        handleSample( lastOrientation )

def loop():
    global disp
    global sleepTime
    disp.clear().update()
    orientation = bhi160.BHI160Orientation( sample_rate=2 )
    try:
        while 42 == 42:
            utime.sleep_ms( sleepTime )
            updateButtons()
            ckeckFlashLight()
            checkDimAjustment()
            checkDIYbutton()
            checkOrientation( orientation )
    except:
        print("calling orientatin.close()")
        orientation.close()

loop()


My app for the card10 (badge from the last chaos communication camp) with all the functions i need to survive.

already implemented:

- Watch
- Torch

to use my app without modifying the software, you need to modify the hardware (like I did):

- Solder a little switch to GPIO 4
- Solder a little button to GPIO 3

the button shows the time of day while you press it. the switch tuns on the upper LEDs to use it as a torch. if you have problems with the soldering, just contact me.